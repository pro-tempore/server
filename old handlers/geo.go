package handlers

import (
	"log"
	"net/http"

	"gitlab.com/pro-tempore/server/db"
	"gitlab.com/pro-tempore/server/types"
)

const (
	insertGeo = "INSERT INTO geodata (operation, time, latitude, longitude, email) VALUES ($1, $2, $3, $4, $5)"
	geoSchema = `CREATE TABLE geodata (operation text, time text, latitude text, longitude text, email text)`
)

func Geo(w http.ResponseWriter, r *http.Request) {
	log.SetPrefix("[Handlers.Geo]")
	switch r.Method {
	case "POST":

		req := new(types.Geodata)
		if err := structFromRequest(req, r); err != nil {
			log.Println("Failed to extract type from request:", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		if validateToken(req.Token) {
			// TODO: Validate
			log.Println("VALID TOKEN")
			err := db.Exec(insertGeo, req.Operation, req.Time, req.Latitude, req.Longitude, req.Email)
			if err != nil {
				log.Println("Failed to insert data:", err)
			}

			w.WriteHeader(http.StatusOK)
		} else {
			log.Println("NOT VALID TOKEN")
			w.WriteHeader(http.StatusBadRequest)
		}

	case "GET":
		log.Println("Received GET request!")

	default:
		w.WriteHeader(http.StatusBadRequest)
	}
}
