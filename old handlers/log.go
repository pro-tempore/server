package handlers

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/pro-tempore/server/db"
	"gitlab.com/pro-tempore/server/types"
)

const GET_LATEST_DATA = "SELECT * FROM geodata ORDER BY time ASC LIMIT 10"

func Log(w http.ResponseWriter, r *http.Request) {
	log.SetPrefix("[Handlers.Log] ")

	var dataList []types.Geodata
	err := db.Select(&dataList, GET_LATEST_DATA)
	if err != nil {
		log.Println("Failed to query database:", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	response, err := json.Marshal(dataList)
	if err != nil {
		log.Println("Failed to marshal data:", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}
