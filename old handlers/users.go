package handlers

import (

	//"github.com/gorilla/mux"
	//"github.com/gorilla/sessions"
	//"gitlab.com/pro-tempore/server/db"
	"gitlab.com/pro-tempore/server/types"
	"log"
	"net/http"
)

const (
	//insertGeo = "INSERT INTO geodata (operation, time, latitude, longitude, email) VALUES ($1, $2, $3, $4, $5)"
	GET_USERS        = "SELECT * FROM users WHERE company=$1"
	GET_CORP         = "SELECT company FROM users WHERE email=$1"
	GET_ROLE_COMPANY = "SELECT role, company FROM users WHERE email=$1"
)

func Users(w http.ResponseWriter, r *http.Request) {
	log.SetPrefix("[Handlers.Users]")
	switch r.Method {
	case "GET":

		session, err := store.Get(r, "user")
		if err != nil {
			log.Println("error")
			return
		}
		email := session.Values["email"]
		//role := session.Values["role"]

		//log.Println("email: ", email)

		if email == "" {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		emailVal, ok := email.(string)
		if !ok {
			log.Println("not ok")
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		// GET ADMIN CORPORATION
		user := types.User{}
		err = db.QueryRow(GET_ROLE_COMPANY, emailVal).Scan(&user.Role, &user.Company)

		if err != nil {
			log.Println("error getting role and/or company for admin", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		if user.Role != "admin" {
			w.WriteHeader(http.StatusForbidden)
			return
		}

		log.Println("CorP", user.Company)
		log.Println("RolE", user.Role)

		//r.ParseForm()
		//email := r.FormValue("email")

		// GET ALL USERS IN COMPANY IF YOU HAVE ROLE ADMIN AND USE YOUR EMAIL

		//log.Println("email: " + email)
		if email == "" {
			w.WriteHeader(http.StatusNotFound)
			return
		}

		w.WriteHeader(http.StatusOK)

	default:
		w.WriteHeader(http.StatusBadRequest)
	}

}

func getUsersForAdmin(company string) (users []types.User, err error) {

	// KVAR ATT GÖRA - use ska innehålla alla users. Detta tror jag inte funkar.
	use := []types.User{}

	err = db.Get(&use, GET_USERS, company)
	if err != nil {
		log.Println("Database error: ", err)
		return use, err
	}
	if len(use) == 0 {
		log.Println("No user with company", company)
		return use, err
	}
	//log.Println("COMPANY: ", co)
	log.Println(use)
	return use, nil
}

// func getUserInformationComplete(email string) (userInfo []string, err error) {
// 	//log.Println("info get")

// 	err = db.Get(&userInfo, GET_USERS, email)

// 	if err != nil {
// 		log.Println("Database error:", err)
// 		return "", "", err
// 		//code = http.StatusInternalServerError
// 	}
// 	if len(name) == 0 && err == nil {
// 		log.Println("No user with email", email)
// 		return "", "", err
// 		//code = http.StatusNotFound
// 	}
// 	err = db.Get(&role, GET_ROLE, email)
// 	if err != nil {
// 		log.Println("Database error:", err)
// 		return "", "", err
// 		//code = http.StatusInternalServerError
// 	}
// 	if len(role) == 0 && err == nil {
// 		log.Println("No user with email", email)
// 		return "", "", err
// 		//code = http.StatusNotFound
// 	}
// 	//log.Println(verify)
// 	//log.Println("info get end")
// 	log.Println("Name: " + name + " Role: " + role)
// 	return name, role, nil
// }
