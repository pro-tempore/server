CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;
COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';

SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

CREATE TABLE geodata (
    operation text,
    "time" text,
    latitude text,
    longitude text,
    email text
);

CREATE TABLE organization (
    orgname text DEFAULT 'Default Name'::text NOT NULL,
    orgmail text DEFAULT 'Default Name'::text NOT NULL
);

CREATE TABLE roles (
    names text NOT NULL,
    description text,
    modules text NOT NULL
);

CREATE TABLE schedule_events (
    username text,
    uid text,
    orgname text,
    location text,
    summary text,
    starttime text,
    endtime text
);

CREATE TABLE users (
    name text NOT NULL,
    lastname text NOT NULL,
    password text NOT NULL,
    email text NOT NULL,
    company text NOT NULL,
    role text NOT NULL,
    registertime text NOT NULL,
    phone text,
    address text,
    address2 text,
    city text,
    region text,
    zip text,
    country text,
    uid integer NOT NULL
);

CREATE SEQUENCE users_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE users_uid_seq OWNED BY users.uid;
ALTER TABLE ONLY users ALTER COLUMN uid SET DEFAULT nextval('users_uid_seq'::regclass);
