        $(document).ready(function() {
            $('#registerbutton').click(function(x){
                x.preventDefault();
                var email = $('#email').val();
                var name = $('#name').val();
                var lastname = $('#lastname').val();
                var company = $('#company').val();
                var role =$('#role').val();
                var password = $('#password').val();
                var passConfirm = $('#passConfirm').val();


                var jsonData = {
                    "email" : email,
                    "name" : name,
                    "lastname": lastname,
                    "company": company,
                    "role": role,
                    "password" : password,
                    "passConfirm" : passConfirm
                }
                console.log(jsonData)
                    $.ajax({
                    type: 'POST',
                    contentType: "application/json",
                    url: 'http://localhost:30000/api/v1/register',
                    data: JSON.stringify(jsonData),
                    crossDomain: true,
                    success: function(data) {
                        console.log(data);

                        $.cookie("Token", data.Token, {
                            expires: 30
                        });
                        //$.cookie("Username", data.Username, {expires: 30});

                        //console.log($.cookie("Username"));
                        //console.log($.cookie("Token"));

                    },
                    error: function(data) {
                        console.log("ERROR")
                        console.log(data)
                    }
                });
            
            });    
                
        });
        