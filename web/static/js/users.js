
function getUsers(){
   $(document).ready(function(){
    $.ajax({
        type: 'GET',
        contentType: "application/json",
        url: '/api/v1/users',
        //data: JSON.stringify(jsonData),
        crossDomain: true,
        success: function(data) {
            console.log(data);
            return data;
        },
        error: function(data) {
            console.log(data);
            return data;
        }
    });

   });
    
}


window.operateEvents = {
       'click .edit': function (e, value, row) {
            window.location.href = '/users/delete'+JSON.stringify(row.Uid);
            
            //alert('You click edit action, row: ' + JSON.stringify(row.Uid));
        },
        'click .remove': function (e, value, row) {
            window.location.href = '/users/edit/'+JSON.stringify(row.Uid);
            //alert('You click remove action, row: ' + JSON.stringify(row.Uid));
        }


        
    };


 function operateFormatter(value, row, index) {
        return [
            '<a class="edit" href="/users/' + JSON.stringify(row.Uid) + '" title="Edit">',
            '<i class="fa fa-pencil-square-o iconcolor"></i>',
            '</a>  ',
            '<a class="remove" href="/users/delete'+JSON.stringify(row.Uid)+'" title="Remove">',
            '<i class="fa fa-trash iconcolor"></i>',
            '</a>'
        ].join('');
    }