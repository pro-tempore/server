        $(document).ready(function() {
            $('#submitbutton').click(function(x){
                x.preventDefault();
                var email = $('#email').val();
                var password = $('#password').val();

                var jsonData = {
                    "email" : email,
                    "password" : password
                }
                //console.log(jsonData)
                    $.ajax({
                    type: 'POST',
                    contentType: "application/json",
                    url: '/api/v1/login',
                    data: JSON.stringify(jsonData),
                    crossDomain: true,
                    success: function(data) {
                        //console.log($.cookie());
                        //console.log($.cookie("name"));
                        window.location.href = '/';
                    },
                    error: function(data) {
                        errorhandler(data.status);
                        
                    }
                });
            
            });
                
        });
        

        $(document).ready(function() {
            $('#logoutbutton').click(function(x) {
                x.preventDefault();
                console.log("logout");
                 $.ajax({
                    type: 'GET',
                    contentType: "application/json",
                    url: '/api/v1/logout',
                    //data: JSON.stringify(jsonData),
                    crossDomain: true,
                    success: function(data) {
                        window.location.href = '/login';
                    },
                    error: function(data) {
                        console.log(data)
                    }
                });
            });
        });



function errorhandler(errorcode) {
    if(errorcode == 401 || 404){  
    swal({ 
      title: 'error',
         text: 'The username or password you entered is not valid',
            type: 'error',
               confirmButtonText: 'OK'
     });

    }

}
