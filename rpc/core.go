package rpc

import (
	"errors"
	"log"
	"net/rpc"

	"gitlab.com/pro-tempore/server/types"
)

type Core struct {
	active map[string]string
}

var (
	running bool
	core    *Core
)

func GetInstance() *Core {
	if !running {
		core = new(Core)
		core.active = make(map[string]string)
		running = true
	}
	return core
}

func (r *Core) Get(name string, out *string) error {
	tmp, ok := r.active[name]
	if !ok {
		return errors.New("No such module: \"" + name + "\"")
	}
	*out = tmp
	return nil
}

func (r *Core) IsActive(name string, ok *bool) error {
	_, *ok = r.active[name]
	return nil
}

func (r *Core) List(none string, out *[]string) error {
	var keys []string
	for k := range r.active {
		keys = append(keys, k)
	}

	*out = keys
	return nil
}

func (r *Core) Register(id types.Identity, out *bool) error {
	log.SetPrefix("[Rpc.Core] ")
	log.Println("Register call from " + id.Address)
	if address, ok := r.active[id.Name]; ok {
		err := errors.New("Module \"" + id.Name + "\" is already registered at " + address + "!")
		log.Println(err)
		return err
	}
	r.active[id.Name] = id.Address
	*out = true
	log.Printf("Registered module \"%s\" at %s.", id.Name, id.Address)
	return nil
}

func (r *Core) Deregister(id types.Identity, out *bool) error {
	log.SetPrefix("[Rpc.Core] ")
	address, ok := r.active[id.Name]
	if !ok {
		return errors.New("No such module: \"" + id.Name + "\"")
	}
	if address != id.Address {
		return errors.New("Request address does not match stored address.")
	}
	delete(r.active, id.Name)
	*out = true
	log.Printf("Deregistered module \"%s\"", id.Name)
	return nil
}

func (c *Core) Shutdown() {
	log.Println("Telling modules to stop...")

	for k, v := range c.active {
		log.Println("Stopping", k+"...")
		client, _ := rpc.Dial("tcp", v)

		token := "a token"
		var response bool
		client.Call(k+".Shutdown", token, &response) // TODO: validate this in some way

	}
}
