package module

import (
	"bytes"

	"github.com/gorilla/mux"
	//"github.com/gorilla/sessions"
	"gitlab.com/pro-tempore/server/handlers"
	r "gitlab.com/pro-tempore/server/rpc"
	"gitlab.com/pro-tempore/server/types"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"net/rpc"
	//"strconv"
	"time"
)

var (
	rpcCore     *r.Core
	maxFileSize int64 = 256000 //
	//base              = template.Must(template.ParseFiles("templates/base.gohtml"))
)

func InitHandler() {
	rpcCore = r.GetInstance()
}

func Handler(rw http.ResponseWriter, req *http.Request) {
	log.SetPrefix("[Module.Handler] ")

	// here we fetch the cookie-store from the user.
	sessionStore := handlers.GetStore()
	Cookiestore, err := sessionStore.Get(req, "user")
	userName := Cookiestore.Values["name"]
	userNameString, noError := userName.(string)
	if !noError {
		http.Redirect(rw, req, "/", 301)
		return
	}
	// Checks the cookie for the correct token,
	token := req.Header.Get("Authorization")
	var ok bool
	if token == "" {
		// If there exist no cookie, check the authorization value for a valid token.
		token, ok = Cookiestore.Values["token"].(string)
	}
	//_, ok := token.(string)
	// if there are any error or token still is null, ERROR
	if err != nil || token == "" || !ok {
		http.Redirect(rw, req, "/", 301)
		return
	}
	// Check if the token is valid or not.
	validToken := handlers.ValidateToken(token)
	if !validToken {
		http.Redirect(rw, req, "/", 301)
		return
	} else {
		log.Println("User successfully logged in")
	}

	vars := mux.Vars(req)
	module := vars["module"]
	method, ok := vars["method"]
	if !ok {
		method = "Root"
	}
	var addr string
	err = rpcCore.Get(module, &addr)

	if err != nil {
		log.Printf("Couldn't get module \"%s\": %s", module, err)
		errorPage(rw, http.StatusMovedPermanently, userNameString, "Couldn't find module")
		//rw.WriteHeader(http.StatusBadRequest)
		return
	}

	client, err := rpc.Dial("tcp", addr)
	if err != nil {
		log.Println("Failed to connect to module:", err)
		errorPage(rw, http.StatusInternalServerError, userNameString, "Failed to connect to module")
		//rw.WriteHeader(http.StatusInternalServerError)
		return
	}

	req.ParseMultipartForm(maxFileSize)
	err = req.ParseForm()
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		log.Println("Failed to read request body:", err)
		errorPage(rw, http.StatusBadRequest, userNameString, "Failed to read request body")
		//rw.WriteHeader(http.StatusBadRequest)
		return
	}

	rpcReq := types.NewRpcRequest(body, req.Form, Cookiestore.Values)

	function := module + "." + method
	response := new(types.RpcResponse)
	err = client.Call(function, rpcReq, response)
	if err != nil {
		log.Printf("Call error from %s(): %s", function, err)
		errorMsg := "Call error from " + function
		errorPage(rw, http.StatusInternalServerError, userNameString, errorMsg)
		//rw.WriteHeader(http.StatusInternalServerError)
		return
	}

	t, err := template.New("content").Parse(string(response.Body))
	if err != nil {
		log.Println("Problem with parsing the response.Body")
		errorPage(rw, http.StatusInternalServerError, userNameString, "Problem with parsing the response.Body")
		//rw.WriteHeader(http.StatusInternalServerError)
		return
	}
	tmpl, err := t.ParseFiles("templates/base.gohtml")
	if err != nil {
		errorPage(rw, http.StatusInternalServerError, userNameString, "Problem with parsing the base.gohtml template")
		log.Println("Error parsing base.gohtml template", err)
	}

	var moduleList []string
	err = rpcCore.List("", &moduleList)

	for _, module := range moduleList {
		log.Println(module)
	}

	varmap := map[string]interface{}{
		"modules": moduleList,
		"content": string(response.Body),
		"token":   "some token",
		"name":    userName,
	}

	rw.WriteHeader(response.StatusCode)
	log.Println(response.StatusCode)
	if response.Header.Get("Content-Type") != "" {
		log.Println(response.Header.Get("Content-Type"))
		for k := range response.Header {
			rw.Header().Set(k, response.Header.Get(k))
		}
		http.ServeContent(rw, req, "", time.Now(), bytes.NewReader(response.Body))
	} else {
		tmpl.ExecuteTemplate(rw, "base", varmap)
	}
}

// Generates the base.gohtml + errorlog. so it shows correctly
func errorPage(rw http.ResponseWriter, Statuscode int, userName, ErrorDesc string) {
	log.SetPrefix("[module.Handler][errorPage]")
	var moduleList []string
	fatalErr := rpcCore.List("", &moduleList)
	if fatalErr != nil {
		log.Fatalln(" Problem reading rpcCore.List")
	}
	varmap := map[string]interface{}{
		"name":             userName,
		"modules":          moduleList,
		"statuscode":       Statuscode,
		"Error":            http.StatusText(Statuscode),
		"Errordescription": ErrorDesc,
	}
	templ, _ := template.ParseFiles("templates/404.gohtml")
	t, err := templ.ParseFiles("templates/base.gohtml")
	if err != nil {
		log.Fatalln("Couldn't load the Error template: ", err)
	}
	t.ExecuteTemplate(rw, "base", varmap)
	return
}
