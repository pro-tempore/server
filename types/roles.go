package types

type Roles struct {
	Name        string
	Description string
	Modules     string
}
