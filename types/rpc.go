package types

import (
	"net/http"
	"net/url"
)

type RpcResponse struct {
	Body       []byte
	StatusCode int
	Header     http.Header
}

type RpcRequest struct {
	Body    []byte
	Form    url.Values
	Cookies map[string]interface{}
}

func NewRpcRequest(b []byte, f url.Values, c map[interface{}]interface{}) RpcRequest {
	cookie := make(map[string]interface{})
	for k, v := range c {
		cookie[k.(string)] = v
	}
	return RpcRequest{Body: b, Form: f, Cookies: cookie}
}
