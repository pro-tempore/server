package types

type DbQuery struct {
	Cmd  string
	Args []interface{}
}

func NewQuery(cmd string, args ...interface{}) DbQuery {
	return DbQuery{Cmd: cmd, Args: args}
}
