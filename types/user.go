package types

type User struct {
	Uid          int64
	Name         string
	Lastname     string
	Password     []byte
	Email        string
	Company      string
	Role         string
	Registertime string
	Phone        string
	Address      string
	Address2     string
	City         string
	Region       string
	Zip          string
	Country      string
}
