package types

type Events struct {
	Username  string
	Uid       string
	Orgname   string
	Starttime string
	Endtime   string
	Location  string
	Summary   string
}
