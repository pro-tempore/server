package types

import (
	_ "database/sql"
	_ "github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type Geodata struct {
	Operation string
	Time      string
	Latitude  string
	Longitude string
	Email     string
}

/*
func (g *Geodata) Get() *Geodata {
	return g
}*/
