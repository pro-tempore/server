package types

type LoginMsg struct {
	Email    string
	Password string
}

type RegisterMsg struct {
	Name        string
	Lastname    string
	Password    string
	PassConfirm string
	Email       string
	Company     string
	Role        string
	Phone       string
	Address     string
	Address2    string
	City        string
	Region      string
	Zip         string
	Country     string
}

type AuthMsg struct {
	Token string
}
