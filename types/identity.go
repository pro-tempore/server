package types

type Identity struct {
	Name    string
	Address string
}
