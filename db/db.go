package db

import (
	_ "database/sql"
	"flag"
	"fmt"
	"log"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/pro-tempore/server/types"
)

var (
	USER     = flag.String("dbuser", "REQUIRED", "User name for the database connection")
	PASSWORD = flag.String("dbpass", "", "Password for the database connection")
	DBNAME   = flag.String("db", "REQUIRED", "Database table to use")
	SSLMODE  = flag.String("ssl", "", "Use SSL for database connection. \n\tdefault - No SSL\n\t\"require\" - Always SSL (skip verification)\n\t\"verify-ca\" - Always SSL (verify that the certificate presented by the server was signed by a trusted CA)\n\t\"verify-full\" - Always SSL (verify that the certification presented by the server was signed by a trusted CA and the server host name matches the one in the certificate)")
	HOST     = flag.String("dbhost", "127.0.0.1:5432", "IP address to the database")
)

type RpcDb struct {
}

var (
	db       *sqlx.DB
	instance RpcDb
	running  bool
)

func main() {
	if *USER == "REQIURED" || *DBNAME == "REQUIRED" {
		log.Fatal("Flags not correctly set! Use the --help flag to check required flags.")
	}
	if *SSLMODE == "" {
		*SSLMODE = "disabled"
	}

	var err error
	db, err = sqlx.Connect(
		"postgres",
		fmt.Sprintf(
			"host=%s user=%s password=%s dbname=%s sslmode=%s",
			*HOST,
			*USER,
			*PASSWORD,
			*DBNAME,
			SSLMODE,
		),
	)
	if err != nil {
		log.Println("Database connection failed:", err)
	}
}

func GetInstance() *RpcDb {
	if !running {
		running = true
		instance = RpcDb{}
	}
	return &instance
}

func (r *RpcDb) Query(in types.DbQuery, out *[]map[string]interface{}) (err error) {
	rows, err := db.Queryx(in.Cmd, in.Args...)
	if err != nil {
		return
	}
	for rows.Next() {
		row := make(map[string]interface{})
		err = rows.MapScan(row)
		if err != nil {
			return
		}
		*out = append(*out, row)
	}
	return
}

func parseList(list []string) (out []interface{}) {
	// still ugly as all hell, but apparently this is how you do it...
	for _, item := range list {
		out = append(out, item)
	}
	return
}

func (r *RpcDb) Shutdown() {
	log.Println("Closing database connection...")
	db.Close()

}
