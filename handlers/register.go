package handlers

import (
	"fmt"
	//"gitlab.com/pro-tempore/server/db"
	"gitlab.com/pro-tempore/server/types"
	"golang.org/x/crypto/bcrypt"
	"log"
	"net/http"
	//"net/rpc"
	"time"
)

const (
	GET_USER_EMAIL  = "SELECT email FROM users WHERE email=$1"
	REGISTER_STRING = "INSERT INTO users (name, lastname, password, email, company, role,  registertime, phone, address, address2, city, region, zip, country) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)"
	//USER_SCHEMA     = `CREATE TABLE users (name text, lastname text, password text, email text, company text, role text, registertime text)`
)

func Register(w http.ResponseWriter, r *http.Request) {
	log.SetPrefix("[Handlers.Register] ")

	if r.Method != "POST" {
		log.Println("Received non-POST request.")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	req := new(types.RegisterMsg)
	if err := structFromRequest(req, r); err != nil {
		log.Println("Failed to extract type from request:", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if req.Name == "" || req.Password == "" {
		log.Println("Username or password empty.")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if req.Password != req.PassConfirm {
		// TODO: Is this needed? Could be done clientside. Svar: NO NOT CLIENT SIDE! DONT TRUST THE USER :@
		log.Println("Password doesn't match confirmation.")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	//dB := db.GetInstance()

	var dest string

	// query := types.DbQuery{
	// 	Cmd:  GET_USER_EMAIL,
	// 	Args: []string{req.Email},
	// }

	//err := dB.Get(query, &dest)
	err = db.Get(&dest, GET_USER_EMAIL, req.Email)
	if dest == req.Email {
		fmt.Println("Email already exist...")
		w.WriteHeader(403)
		return
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.DefaultCost)
	if err != nil {
		log.Println("Failed to generate password hash:", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// query = types.DbQuery{
	// 	Cmd:  REGISTER_STRING,
	// 	Args: []string{req.Name, req.Lastname, string(hash), req.Email, req.Company, req.Role, time.Now().String(), req.Phone, req.Address, req.Address2, req.City, req.Region, req.Zip, req.Country},
	// }
	//	Args []string{req.Name, req.Lastname, string(hash), req.Email, req.Company, req.Role, time.Now().String(), req.Phone, req.Address, req.Address2, req.City, req.Region, req.Zip, req.Country}
	args := []string{req.Name, req.Lastname, string(hash), req.Email, req.Company, req.Role, time.Now().String(), req.Phone, req.Address, req.Address2, req.City, req.Region, req.Zip, req.Country}
	//var reply bool
	//err = client.Call("DB.Exec", query, &reply)
	//	err = dB.Exec(query, &reply)
	shim := shim(args)
	_, err = db.Exec(REGISTER_STRING, shim...)
	//err = db.Exec(REGISTER_STRING, req.Name, req.Lastname, hash, req.Email, req.Company, req.Role, time.Now())
	if err != nil {
		log.Println("Failed to insert into database:", err)
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		fmt.Println("registered user: " + req.Email)
		w.WriteHeader(http.StatusOK)
	}
}
