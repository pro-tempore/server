package handlers

import (
	//"github.com/gorilla/sessions"
	r "gitlab.com/pro-tempore/server/rpc"
	"html/template"
	"log"
	"net/http"
)

var (
	rpcCore *r.Core
)

//var base = template.Must(template.ParseFiles("templates/base.gohtml"))

func Static(url string) func(http.ResponseWriter, *http.Request) {
	return func(rw http.ResponseWriter, req *http.Request) {
		//Check if token valid.
		if url == "login" {
			t, _ := template.ParseFiles("web/login.html")
			t.Execute(rw, nil)
			return
		}

		var token string
		session, err := store.Get(req, "user")
		if err != nil {
			http.Redirect(rw, req, "/login", http.StatusMovedPermanently)
			return
		}

		myToken := session.Values["token"]
		if myToken != nil {
			token = myToken.(string)
		}
		if token == "" || !ValidateToken(token) {
			http.Redirect(rw, req, "/login", http.StatusMovedPermanently)
			return
		}

		// If token validates. then serve the static website
		userName := session.Values["name"]
		userNameString, noError := userName.(string)
		if !noError {
			http.Redirect(rw, req, "/login", http.StatusMovedPermanently)
			return
		}
		var moduleList []string
		rpcCore := r.GetInstance()
		fatalErr := rpcCore.List("", &moduleList)
		if fatalErr != nil {
			log.Fatalln(" Problem reading rpcCore.List")
		}
		varmap := map[string]interface{}{
			"name":    userNameString,
			"modules": moduleList,
		}
		templ, _ := template.ParseFiles("templates/index.gohtml")
		t, err := templ.ParseFiles("templates/base.gohtml")
		if err != nil {
			log.Fatalln("Couldn't load the Error template: ", err)
		}
		t.ExecuteTemplate(rw, "base", varmap)
		return
	}
}
