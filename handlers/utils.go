package handlers

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/dgrijalva/jwt-go"
)

func structFromRequest(item interface{}, r *http.Request) error {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}
	err = json.Unmarshal(body, item)
	return err
}

func ValidateToken(myToken string) bool {
	log.Println("Checking if token valid")
	token, err := jwt.Parse(myToken, func(token *jwt.Token) (interface{}, error) {
		return publicKey, nil
	})
	if err != nil {
		return false
	}
	return token.Valid
}
