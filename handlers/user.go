package handlers

import (
	"github.com/gorilla/mux"
	//"github.com/gorilla/sessions"
	//"gitlab.com/pro-tempore/server/db"
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/pro-tempore/server/types"
)

const (
	GET_USERINF = "SELECT name, lastname, email, company, role, registertime, phone, address, city, region, zip, country FROM users WHERE uid=$1"
)

func User(w http.ResponseWriter, r *http.Request) {
	log.SetPrefix("[Handlers.User]")
	switch r.Method {
	case "GET":

		vars := mux.Vars(r)
		uid := vars["uid"]
		log.Println("UID", uid)
		// Try to get key "/users/1" <- key is 1. Then we need to get user with id 1

		ok := isAdminCheck(w, r)
		if !ok {
			w.WriteHeader(http.StatusForbidden)
			return
		}

		userInf, err := getUserInf(uid)
		if err != nil {
			log.Println("error getitng userinf", err)
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(userInf)
		w.WriteHeader(http.StatusOK)

	default:
		w.WriteHeader(http.StatusBadRequest)
	}

}

func isAdminCheck(w http.ResponseWriter, r *http.Request) bool {
	session, err := store.Get(r, "user")
	if err != nil {
		log.Println("error")
		return false
	}
	email := session.Values["email"]

	if email == "" {
		//w.WriteHeader(http.StatusBadRequest)
		return false
	}

	emailVal, ok := email.(string)
	if !ok {
		log.Println("not ok")
		return false
	}
	// GET ADMIN CORPORATION
	user := types.User{}
	err = db.QueryRow(GET_ROLE_COMPANY, emailVal).Scan(&user.Role, &user.Company)

	if err != nil {
		log.Println("error getting role and/or company for admin", err)
		return false
	}
	if user.Role != "admin" {
		return false
	}
	return true
}

func getUserInf(uid string) (userInfo []byte, err error) {

	// KVAR ATT GÖRA - use ska innehålla alla users. Detta tror jag inte funkar.
	user := types.User{}
	//userByte := types.User{}
	//var data []byte

	rows, err := db.Queryx(GET_USERINF, uid)
	if err != nil {
		log.Println("error getting users from db", err)
	}
	for rows.Next() {

		err = rows.StructScan(&user)
		if err != nil {
			log.Println("erroro structscan", err)
		}

		//log.Println(user)

	}
	//log.Println(userArray)
	//err = json.Marshal(userArray, &data)
	b, _ := json.Marshal(user)

	return b, nil
}
