package handlers

import (
	//"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/sessions"
	//"net/rpc"
	"time"
	//"github.com/auth0/go-jwt-middleware"
	"github.com/dgrijalva/jwt-go"
	//"gitlab.com/pro-tempore/server/db"
	//"github.com/jmoiron/sqlx"
	"gitlab.com/pro-tempore/server/types"
	"golang.org/x/crypto/bcrypt"
)

var (
	privateKey, _ = ioutil.ReadFile("demo.rsa")
	publicKey, _  = ioutil.ReadFile("demo.rsa.pub")
	store         = sessions.NewCookieStore([]byte("hakunamatata"))
	RPCPort       = ":30001" // RPC port on the main.Handler
	DBPort        = ":30002" // DB handler port
	//server = flag.String("server", "", "localhost") // used by calling "mymodule -server 123.123.123.123"
	//db *sqlx.DB
)

const (
	VALID_HOURS           = 72
	GET_USER              = "SELECT * FROM users WHERE email=$1"
	GET_USER_ID_NAME_ROLE = "SELECT uid, name, role FROM users WHERE email=$1"
	GET_HASH              = "SELECT password FROM users WHERE email=$1"
	GET_NAME              = "SELECT name FROM users WHERE email=$1"
	GET_ROLE              = "SELECT role FROM users WHERE email=$1"
)

func Login(w http.ResponseWriter, r *http.Request) {
	log.SetPrefix("[Handlers.Login] ")

	// DATABASE PART REMOVE IF RPC WORKS

	//-----------------------------------

	req, ok := validateRequest(r)
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if status := validateHash(req, req.Email); status != 0 {
		w.WriteHeader(status)
		return
	}
	log.Println("Credentials match.")

	// Create cookie and send back
	session, err := store.Get(r, "user")
	if err != nil {
		log.Println("error")
		return
	}

	uid, name, role, err := getUserInformation(req.Email)

	session.Values["email"] = req.Email
	session.Values["name"] = name
	session.Values["uid"] = uid
	session.Values["role"] = role

	log.Println("UID", uid)

	if err != nil {

		session.Values["name"] = name
		w.WriteHeader(http.StatusForbidden)
		return
	}

	token, status := generateToken(req.Email, role)
	if status != 0 {
		w.WriteHeader(status)
		return
	}

	session.Values["token"] = token
	session.Save(r, w)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(`{"token": "` + token + `"}`))
}
func getUserInformation(email string) (uid int64, name string, role string, err error) {
	log.Println("get info")
	user := types.User{}

	//DATABASE CALL (direct access)

	err = db.QueryRow(GET_USER_ID_NAME_ROLE, email).Scan(&user.Uid, &user.Name, &user.Role)

	if err != nil {
		log.Println("Database error:", err)
		return 0, "", "", err
		//code = http.StatusInternalServerError
	}
	if user.Name == "" {
		log.Println("No user with email", email)
		return 0, "", "", err
		//code = http.StatusNotFound
	}
	return user.Uid, user.Name, user.Role, nil
}

func validateRequest(r *http.Request) (req types.LoginMsg, ok bool) {
	if r.Method != "POST" {
		log.Println("Received non-POST request.")
		return
	}

	if err := structFromRequest(&req, r); err != nil {
		log.Println("Failed to extract type from request:", err)
		return
	}

	if req.Email == "" || req.Password == "" {
		log.Println("Username or password empty.")
		return
	}
	ok = true
	return
}

func validateHash(req types.LoginMsg, email string) (code int) {

	var verify []byte

	err = db.Get(&verify, GET_HASH, email)
	if err != nil {
		log.Println("Database error:", err)
		code = http.StatusInternalServerError
	}
	if len(verify) == 0 {
		log.Println("No user with email", req.Email)
		code = http.StatusNotFound
	}

	if code != 0 {
		return
	}

	err = bcrypt.CompareHashAndPassword(verify, []byte(req.Password))
	if err != nil {
		log.Print("User entered incorrect password.")
		code = http.StatusUnauthorized
	}
	return
}

func generateToken(name string, role string) (out string, code int) {
	token := jwt.New(jwt.SigningMethodRS256)

	//set some claims
	token.Claims["name"] = name
	token.Claims["role"] = role
	token.Claims["exp"] = time.Now().Add(time.Hour * VALID_HOURS).Unix()

	//sign and get the complete encoded token as a string
	out, err := token.SignedString(privateKey)
	if err != nil {
		log.Println("Failed to generate token string:", err)
		code = http.StatusInternalServerError
	}
	return
}

func GetStore() *sessions.CookieStore {
	return store
}
