package handlers

import (

	//"github.com/gorilla/mux"
	//"github.com/gorilla/sessions"
	//"gitlab.com/pro-tempore/server/db"
	"encoding/json"
	"gitlab.com/pro-tempore/server/types"
	"log"
	"net/http"
)

const (
	//insertGeo = "INSERT INTO geodata (operation, time, latitude, longitude, email) VALUES ($1, $2, $3, $4, $5)"
	GET_USERS        = "SELECT uid, name, lastname, email, company, role, registertime, phone, address, address2, city, region, zip, country FROM users WHERE company=$1"
	GET_CORP         = "SELECT company FROM users WHERE email=$1"
	GET_ROLE_COMPANY = "SELECT role, company FROM users WHERE email=$1"
)

func Users(w http.ResponseWriter, r *http.Request) {
	log.SetPrefix("[Handlers.Users]")
	switch r.Method {
	case "GET":

		// Try to get key "/users/1" <- key is 1. Then we need to get user with id 1

		session, err := store.Get(r, "user")
		if err != nil {
			log.Println("error")
			return
		}
		email := session.Values["email"]
		//role := session.Values["role"]

		//log.Println("email: ", email)

		if email == "" {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		emailVal, ok := email.(string)
		if !ok {
			log.Println("not ok")
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		// GET ADMIN CORPORATION
		user := types.User{}
		err = db.QueryRow(GET_ROLE_COMPANY, emailVal).Scan(&user.Role, &user.Company)

		if err != nil {
			log.Println("error getting role and/or company for admin", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		if user.Role != "admin" {
			w.WriteHeader(http.StatusForbidden)
			return
		}

		log.Println("CorP", user.Company)
		log.Println("RolE", user.Role)

		allUsers, err := getUsersForAdmin(user.Company)
		if err != nil {
			log.Println("error getitng users for admin", err)
		}
		//log.Print("user here")
		//log.Println(allUsers)
		w.Header().Set("Content-Type", "application/json")
		w.Write(allUsers)
		w.WriteHeader(http.StatusOK)

	default:
		w.WriteHeader(http.StatusBadRequest)
	}

}

func getUsersForAdmin(company string) (users []byte, err error) {

	// KVAR ATT GÖRA - use ska innehålla alla users. Detta tror jag inte funkar.

	userArray := []types.User{}
	var data []byte

	rows, err := db.Queryx(GET_USERS, company)
	if err != nil {
		log.Println("error getting users from db", err)
		return data, err
	}
	for rows.Next() {
		user := types.User{}
		err = rows.StructScan(&user)
		if err != nil {
			log.Println("erroro structscan", err)
			return data, err
		}

		//log.Println(user)
		userArray = append(userArray, user)

	}
	//log.Println(userArray)
	//err = json.Marshal(userArray, &data)
	b, _ := json.Marshal(userArray)

	return b, nil
}

// func getUserInformationComplete(email string) (userInfo []string, err error) {
// 	//log.Println("info get")

// 	err = db.Get(&userInfo, GET_USERS, email)

// 	if err != nil {
// 		log.Println("Database error:", err)
// 		return "", "", err
// 		//code = http.StatusInternalServerError
// 	}
// 	if len(name) == 0 && err == nil {
// 		log.Println("No user with email", email)
// 		return "", "", err
// 		//code = http.StatusNotFound
// 	}
// 	err = db.Get(&role, GET_ROLE, email)
// 	if err != nil {
// 		log.Println("Database error:", err)
// 		return "", "", err
// 		//code = http.StatusInternalServerError
// 	}
// 	if len(role) == 0 && err == nil {
// 		log.Println("No user with email", email)
// 		return "", "", err
// 		//code = http.StatusNotFound
// 	}
// 	//log.Println(verify)
// 	//log.Println("info get end")
// 	log.Println("Name: " + name + " Role: " + role)
// 	return name, role, nil
// }
