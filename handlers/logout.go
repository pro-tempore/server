package handlers

import (
	"log"
	"net/http"
)

func Logout(w http.ResponseWriter, r *http.Request) {
	log.SetPrefix("Handlers.Logout")
	log.Println("GET to /api/v1/logout")

	session, err := store.Get(r, "user")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	session.Values["token"] = ""
	session.Save(r, w)
	w.WriteHeader(http.StatusOK)
	return
}
