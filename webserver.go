package main

import (
	"flag"
	"log"
	"net"
	"net/http"
	"net/rpc"
	"os"
	"os/signal"
	"syscall"

	"github.com/gorilla/mux"
	"gitlab.com/pro-tempore/server/db"
	"gitlab.com/pro-tempore/server/handlers"
	"gitlab.com/pro-tempore/server/module"
	rpcServer "gitlab.com/pro-tempore/server/rpc"
)

func main() {
	flag.Parse()
	log.SetPrefix("[Main] ")
	rpcInstance := rpcServer.GetInstance()

	module.InitHandler()
	pluginHost := rpc.NewServer()
	pluginHost.RegisterName("Main", rpcInstance)
	pluginListener, err := net.Listen("tcp", ":30001")
	if err != nil {
		log.Fatalln("Failed to start RPC listener:", err)
	}
	go pluginHost.Accept(pluginListener) // blocking, so threaded
	log.Println("Plugin Host active on TCP 30001.")

	dbInstance := db.GetInstance()
	dbHost := rpc.NewServer()
	dbHost.RegisterName("DB", dbInstance)
	dbListener, err := net.Listen("tcp", ":30002")
	if err != nil {
		log.Fatalln("Failed to start database listener:", err)
	}
	go dbHost.Accept(dbListener)
	log.Println("Database Host active on TCP 30002.")

	router := mux.NewRouter()
	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("web/static/"))))
	router.HandleFunc("/api/v1/logout", handlers.Logout)
	router.HandleFunc("/api/v1/login", handlers.Login)
	router.HandleFunc("/api/v1/register", handlers.Register)
	router.HandleFunc("/api/v1/users", handlers.Users)
	router.HandleFunc("/api/v1/users/{uid}", handlers.User)
	router.HandleFunc("/api/v2/{module}", module.Handler)          // module name
	router.HandleFunc("/api/v2/{module}/{method}", module.Handler) // method name, defined by modules

	router.HandleFunc("/", handlers.Static("index"))
	router.HandleFunc("/login", handlers.Static("login"))

	// catch ^C
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	signal.Notify(c, syscall.SIGTERM)
	go func() {
		<-c
		log.Println("Shutting down...")
		rpcInstance.Shutdown()
		dbInstance.Shutdown()
		dbListener.Close()
		pluginListener.Close()
		os.Exit(1)
	}()

	log.Println("Starting HTTP server...")

	err = http.ListenAndServe(":30000", router) // also blocking, but we're done initializing.
	if err != nil {
		log.Fatalln("Failed to start HTTP server:", err)
	}
}
